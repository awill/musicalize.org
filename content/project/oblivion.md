+++
# Date this page was created.
date = "2021-07-01"
draft = false

# Project title.
title = "Oblivion"

# Project summary to display on homepage.
summary = "-"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "thumbnails/oblivion.jpg"

# Tags: can be used for filtering projects.
# Example: `tags = ["classical", "ear"]`
tags = ["ear"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Optional featured image (relative to `static/img/` folder).
[header]
#image = "headers/bubbles-wide.jpg"
#caption = "My caption :smile:"

+++
<br>
{{< youtube id="WXNQAW9TpY0" autoplay="false" >}}