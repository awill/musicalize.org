+++
# About/Biography widget.

date = "2022-08-09"
draft = false

widget = "about"

# Order that this section will appear in.
weight = 2

# List your academic interests.
#[interests]
#  interests = [
#    "Classical Music",
#    "Playing by ear"
#    ]

# List your qualifications (such as academic degrees).

#[[education.courses]]
#  course = "Diploma"
#  institution = "Spišska Nová Ves Music School, Slovakia"
#  year = 2004

#[[education.courses]]
#  course = "Leisure Services Management"
#  institution = "Marriott School of Business, Brigham Young University"
#  year = 2009
 
+++

Denisse initiated her piano studies at age 8 under Mary Bowman, in northern Mexico. In 2004, she moved to Slovakia and studied under Dagmar Schmitová and graduated from the School of Music in Spišská Nová Ves. Denisse later enrolled and graduated from Brigham Young University in 2010 with a B.S. degree in Leisure Services Management and a German Language minor, after which she continued her piano studies with Tatyana Ekshteyn in Salt Lake City, Utah. Denisse is now offering private piano lessons in her home studio in Sammamish, Washington. She also enjoys teaching general music lessons at independent schools and is certified in Conversational Solfege 1 & 2 by the Feierabend Association for Music Education.
