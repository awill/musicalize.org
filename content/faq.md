+++
# FAQ
date = "2022-08-09"
draft = false
#widget = "custom"

# Order that this section will appear in.
weight = 7

title = "Frequently Asked Questions"
+++

**Q:** "Do you teach at students' homes?"  
**A:** Unfortunately not at this time.

**Q:** "Do you offer lessons longer than 30 minutes?"  
**A:** Yes, 45-min and 60-min lessons are also available. 

**Q:** "Do you teach adults?"  
**A:** Yes! I love teaching adults. Learning new skills is not only for the young!

**Q:** "Do you offer a trial lesson?"  
**A:** Absolutely! I encourage all interested students (and/or parents) to come for a trial lesson, especially if they're unsure of what to expect or, in the case of their child, how he/she will respond to piano lessons. This trial lesson tends to go a little longer (about 45 minutes) and is always FREE.
