+++
date = "2022-08-09"
draft = false

title = "Lessons"
subtitle = ""
#widget = "custom"

# Order that this section will appear in.
weight = 2

+++

Lessons are 30, 45, or 60 minutes long and held at Denisse’s home studio on a weekly basis. Please refer to the studio policy for lesson rates, and contact Denisse directly for teaching availability or to schedule a free trial lesson.

![](/img/musicalize_studio-1.jpg)

![](/img/musicalize_studio-2.jpg)

**FROM THE INSTRUCTOR...**

“Thank you for considering Musicalize Piano Studio. I have enjoyed teaching students of all ages (from young children to retired seniors) and abilities. For BEGINNER students (of any age) I recommend using method books. They provide a clear, well-structured and comprehensive path for the learning process, and there are beginner books available for every age group. I currently use and recommend the Faber and Faber method, although I also have experience with the Bastien Method, the Alfred Method, and the Thompson books. Once students have finished level 2 of the Faber Method, I introduce them to graded material from the Royal Conservatory of Music (RCM books) and/or the Associated Board of the Royal Schools of Music (ABRSM books). I complement these books with Keith Snell’s series of theory books and technical works by Hanon and Czerny. Additionally, students work on separate pieces of their choice outside the method books and RCM/ABRSM curriculum. Once students reach the intermediate or advanced level, the material covered in lessons will vary from class to class. Sometimes we may spend more time working on technique, other times on ear training or theory concepts, etc. I encourage ALL students (regardless of their current proficiency level) to choose pieces they would like to learn from a genre they enjoy (movie soundtracks, pop music, etc.). They work on these pieces along their regular studies, and eventually these pieces become recital material. They especially enjoy the “Movie Theme” recitals, where they choose a movie theme to perform as well as dress up as the character of their choice. For traditional recitals, the students work on classical pieces. By doing this they will not only build a diverse repertoire, but also become well-rounded musicians. Musicalize Piano Studio holds two student recitals a year in which students are expected to participate. I also usually play at my student recitals, as I believe it is important as an instructor to show support towards my students by honoring the same standards that I require them to live up to.”