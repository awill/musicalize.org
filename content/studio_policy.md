+++
date = "2022-08-09"
draft = false

title = "Musicalize Piano Studio Policy"
subtitle = ""
#widget = "custom"

# Order that this section will appear in.
weight = 8

+++
You can download the Musicalize Piano Studio Policy [here][0]

[0]:	http://www.musicalize.org/files/Musicalize_Studio_Policy_2024-2025.pdf