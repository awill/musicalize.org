+++
date = "2022-08-09"
draft = false

title = "Photos"
subtitle = ""
#widget = "custom"

# Order that this section will appear in.
weight = 5

+++


![](/img/denisse/music-class.jpg) Music Class
![](/img/denisse/group-lesson-1.jpg) Group Lesson 1
![](/img/denisse/group-lesson-2.jpg) Group Lesson 2

![](/img/denisse/2022-12.jpg) Winter Recital at Faith Church - December 2022
![](/img/denisse/2022-06.jpg) Spring Recital at Pine Lake Covenant Church -June 2022
![](/img/denisse/2023-05.jpg) Spring Recital at Faith Church - May 2023
![](/img/denisse/2019-06.jpg) Spring Movie Theme Recital at Sammamish Presbyterian Church - June 2019
![](/img/denisse/2018-06.jpg) Spring Recital at Northwest Pianos in Bellevue, WA - June 2018
![](/img/denisse/2018-05.jpg) Group lesson - May 2018
![](/img/denisse/2018-02a.jpg) Recording by Sammamish Sounds Recording at Tim Noah Thumbnail Theater - February 2018
![](/img/denisse/2018-02b.jpg) Group lesson - February 2018
![](/img/denisse/2017-12.jpg) Winter Recital at the Sammamish Presbyterian Church - December 2017
![](/img/denisse/2015-05b.jpg) Private event in Barcelona - May 2015
![](/img/denisse/2015-05a.jpg) With piano instructors Alica Faixová (left) and Dagmar Schmidtová (right) in Spišská Nová Ves, Slovakia - May 2015
![](/img/denisse/2015-01.jpg) Student recital at Dayne's Music in Midvale, UT - January 2015
![](/img/denisse/2014-06.jpg) Performance recital with piano instructors Tatyana Ekshteyn and Mary Bowman - June 2014
![](/img/denisse/2008-04.jpg) Private event in Salt Lake City, UT - April 2008
![](/img/denisse/2008-03.jpg) Solo recital for Instituto de Estudios Vallejianos at the Museum of Art, BYU in Provo, UT - March 2008
![](/img/denisse/2004-12c.jpg) Performance at the Spišská Nová Ves Theatre, Slovakia - December 2004
![](/img/denisse/2004-12b.jpg) Kroměříž, Czech Republic - 2004
![](/img/denisse/2004-12a.jpg) Kroměříž, Czech Republic - 2004
![](/img/denisse/2004-11.jpg) With piano instructor Alica Faixová in Slovakia - 2004
![](/img/denisse/1988-01.jpg) Denisse receiving her first toy piano
![](/img/denisse/1987-01.jpg) Denisse around 1 year old
