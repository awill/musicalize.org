+++
# Biography
date = "2022-08-09"
draft = false
#widget = "custom"

# Order that this section will appear in.
weight = 6

title = "Biography"
+++

Denisse initiated her classical piano studies in northern Mexico at the age of 8 under Mary Bowman. Although she wanted to discontinue lessons soon after, her father kindly persuaded her to not give up just yet, and she hasn't stopped playing the piano since. Growing up she was always involved in ensembles, orchestras and bands, learning to play the clarinet, viola and oboe along the way. At age 16 she continued her studies with Armenian instructor Lillit Markarian, faculty at the Conservatory of Music in the Mexican state of Chihuahua. After finishing her high school education, Denisse moved to Slovakia, where she learned Slovak and studied classical piano under Dagmar Schmitová in the Spišská Nová Ves school of music. She graduated with the class of 2005 and earned a place to perform at the Absolventský Konzert (end-of-year concert for top graduating students). During her time in Slovakia, Denisse was also invited to join the Collegium Technicum Choir of Košice and was part of the 2004-2005 tours in Spain, Portugal, and Czech Republic.

After her time in Slovakia, Denisse moved directly to the U.S.A. to pursue undergraduate studies at Brigham Young University, in Provo, UT. She continued her piano studies under Marlene Bachelder while completing a degree in Leisure Services Management from the Marriott School of Business, and a German Language minor. One of her fondest memories of this time is a study abroad in Vienna in 2007, where she enjoyed living in the musical capital of the world for a semester, attending many concerts and countless operas.

After concluding her higher education studies, Denisse embarked on a successful career in the events industry, planning hundreds of weddings, corporate, and family events every year. During this time, she continued her classical piano studies under Tatyana Ekshteyn in Salt Lake City, UT, and was invited to perform at several private functions. Although an event planner by profession, she remains a musician at heart, so she finally started her own piano teaching studio in Lehi, UT. Denisse later moved to the Pacific Northwest and has continued to teach and share her love of music and piano with others. She currently teaches private piano lessons out of her home studio in Sammamish, and general music lessons at local preschools.

When she is not teaching or learning new pieces, she enjoys event planning, fine cooking, and acquiring new language skills.

**IN HER OWN WORDS...**

"I had the privilege of growing up in a musical family where impromptu musical gatherings were a common occurrence. My parents highly valued musical education and understood the important role piano studies have in establishing a solid musical foundation. They asked for at least 2 years of piano lessons of each child, and when it was my turn, it all went smoothly until about 1 year and 2 months into lessons, when I told them I wanted to quit. At that young age I wasn't really able to identify or express the source of my frustration, but my father being the wiser one, asked me to at least carry on until the 18-month mark, and that if I still wanted to discontinue after that, he would allow me to. I agreed to his proposal, and in the following 2-3 months the frustration dissipated and I continued taking lessons for years. In retrospect, I think the frustration I felt at that time was due to the fact that piano was actually becoming harder; I had passed the basic levels and the materials were simply getting more difficult.

The struggles didn't end there, however. I was very diligent with my piano assignments until I was 12-13 years old. At that time I started to show a lot of interest in popular contemporary piano music, and practically spent the following 3-4 years ignoring my lesson books and playing music off CDs instead, extensively developing my ear. Although I deviated from the traditional coursework, my parents and ever-faithful teacher never gave up on me. It was not until age 16 that I got hooked back into classical piano, after attending (as a spectator) a national Edvard Grieg Competition hosted in Mexico. I had the opportunity to watch concert pianists perform during a whole week of semifinals. This experience truly inspired me to pursue classical piano, and I have not stopped ever since. To this day I enjoy playing music from a variety of genres, using sheet music for classical pieces and heavily relying on my ear for everything else.

A few words of encouragement to parents whose children may be going through a rough stage: there is hope! It is true what people say: they WILL thank you when they're older. I am very grateful to my parents for giving me a little push to step out of my comfort zone and not letting me give up so soon. I certainly hope to be a force for good in your child's musical endeavors, whether our paths remain crossed for a long or short period of time. It is my honor to play a part in their musical formation."
