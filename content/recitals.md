+++
# Recitals
date = "2022-08-09"
draft = false
#widget = "custom"

# Order that this section will appear in.
weight = 3

title = "Recitals and Performances"
+++

COMING UP:

**TBD** - 2023 Winter Recital.

PAST EVENTS:

**May 20, 2023** - Spring Recital at Faith Church, Issaquah, WA.

**December 9, 2022** - Winter Recital at Faith Church, Issaquah, WA. 

**June 11, 2022** - Spring Recital at Pine Lake Covenant Church, Sammamish, WA.

**December 18, 2021** - Virtual Winter Recital.

**June 19, 2021** – Virtual Spring Recital.

**December 19, 2020** - Virtual Winter Recital.

**June 26, 2020** - Virtual Spring Recital.

**June 10, 2019** - Spring Recital at Sammamish Presbyterian Church, Sammamish, WA.

**December 10, 2018** - Winter Holiday Recital at Sammamish Presbyterian Church, Sammamish, WA.

**June 1, 2018** - End of Year Recital at Northwest Pianos, Bellevue, WA.

**March 10, 2018** - Sammamish Music Club Junior Festival at Sammamish LDS Church.

**December 11, 2017 at 6pm** - Winter Recital at Sammamish Presbyterian Church.

**June 20, 2017** - Student Recital at Redmond City Library, Redmond, WA.

**January 14, 2015** - Student Recital at Daynes Music, Midvale, UT.

**June 13, 2014** - Performance Recital at Daynes Music, Midvale, UT (video available).

**February 19, 2014** - Performances for the College of Humanities Banquet at Brigham Young University, Provo, UT.

**December 5, 2013** - Performances for the Instituto de Estudios Vallejianos at Brigham Young University, Provo, UT.

**March 21, 2008** - Latin-american Music Recital for the Instituto de Estudios Vallejianos at the Museum of Art, Brigham Young University, Provo, UT.