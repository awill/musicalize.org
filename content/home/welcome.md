+++
# About/Biography widget.

date = "2022-08-09"
draft = false

widget = "custom"

# Order that this section will appear in.
weight = 1

# List your academic interests.
#[interests]
#  interests = [
#    "Classical Music",
#    "Playing by ear"
#    ]

# List your qualifications (such as academic degrees).

#[[education.courses]]
#  course = "Diploma"
#  institution = "Spišska Nová Ves Music School, Slovakia"
#  year = 2004

#[[education.courses]]
#  course = "Leisure Services Management"
#  institution = "Marriott School of Business, Brigham Young University"
#  year = 2009
 
+++

<div style="display:flex;justify-content:center;align-items:center;padding-top: 1cm;">
<div>

<img src="/img/musicalize-logo.png" alt="{{ $alt }}"/>

</div>
</div>

Thank you for visiting Musicalize. Here you will find information about piano lessons, teaching methods, rates, student recitals, videos by Denisse and more. For schedule availability, please contact Denisse directly [here]({{< relref "#contact" >}}).
 
