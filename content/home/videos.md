+++
# Projects widget.
# Note: this widget will only display if `content/project/` contains projects.

date = "2022-08-09"
draft = false

title = "Videos"
subtitle = ""
widget = "projects"

# Order that this section will appear in.
weight = 3

# View.
# Customize how projects are displayed.
# Legend: 0 = list, 1 = cards.
view = 1

# Filter toolbar.
# Add or remove as many filters (`[[filter]]` instances) as you like.
# Use "*" tag to show all projects or an existing tag prefixed with "." to filter by specific tag.
# To remove toolbar, delete/comment all instances of `[[filter]]` below.
[[filter]]
  name = "All"
  tag = "*"
  
[[filter]]
  name = "Classical"
  tag = ".classical"

[[filter]]
  name = "Learned By Ear"
  tag = ".ear"

+++

