+++
# Date this page was created.
date = "2021-07-01"
draft = true

# Project title.
title = "Queen - Don't Stop Me Now"

# Project summary to display on homepage.
summary = "Original arrangement by Peter Bence"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "thumbnails/queen-medley.jpg"

# Tags: can be used for filtering projects.
# Example: `tags = ["classical", "ear"]`
tags = ["ear"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Optional featured image (relative to `static/img/` folder).
[header]
#image = "headers/bubbles-wide.jpg"
#caption = "My caption :smile:"

+++
<br>
{{< youtube id="atdHUX8VAqs" autoplay="false" >}}