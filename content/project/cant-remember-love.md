+++
# Date this page was created.
date = "2021-07-01"
draft = false

# Project title.
title = "Can't Remember Love"

# Project summary to display on homepage.
summary = "From 'The Queen's Gambit'"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "thumbnails/cant-remember-love.jpg"

# Tags: can be used for filtering projects.
# Example: `tags = ["classical", "ear"]`
tags = ["ear"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Optional featured image (relative to `static/img/` folder).
[header]
#image = "headers/bubbles-wide.jpg"
#caption = "My caption :smile:"

+++
<br>
{{< youtube id="_aLEX8eJZQk" autoplay="false" >}}